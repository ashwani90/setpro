import React, {Component} from 'react';
import CreateButton from "../Components/Forms/Buttons/CreateButton";
import DataTable from "../Components/Tables/DataTable";
import { connect } from "react-redux";
import * as actions from '../store/actions/index';
import { Spinner } from 'reactstrap';
import { Redirect } from 'react-router-dom';

const createUser = "Create User";

class UserManager extends Component {

    state = {
        createAction: null,
        updateActionId: null,
        userData: null
    };

    componentDidMount() {
        (this.props.getUsers());
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.userData !== this.state.userData) {
            this.setState(
                {
                    ...this.state,
                    userData: nextProps.userData
                }
            );
        }
    }

    updateActionCalled = (userId) => {
        this.setState(
            {
                ...this.state,
                updateActionId: userId
            }
        );
    };

    createButtonClicked = () => {
        this.setState(
            {
                ...this.state,
                createAction: true
            }
        );
    };

    render () {

        if (this.state.createAction) {
            return (
              <Redirect to={this.props.match.url + "/create"} />
            );
        }

        if (this.state.updateActionId) {
            return (
                <Redirect to={this.props.match.url + "/create/" + this.state.updateActionId} />
            );
        }

        if (this.state.userData !== null) {
            let allColumns = Object.keys(this.state.userData[0]);

            return (
                <div>
                    <CreateButton text={createUser} handleClick={this.createButtonClicked}/>
                    <DataTable allColumns={allColumns} data={this.state.userData}
                               updateAction={(userId) => this.updateActionCalled(userId)}/>
                </div>
            );
        } else {

            return (
                <Spinner color="primary" />
            );
        }

    }
}

const mapDispatchToProps = dispatch => {
    return {
        getUsers: () => dispatch(actions.getUsers()),
    };
};

const mapStateToProps = (state) => {
    return {
        userData: state.users.userData,
        error: state.users.error,
    };
};

export default connect( mapStateToProps , mapDispatchToProps )( UserManager );
