import React, { Component } from 'react';
import classes from './MainComponent.css';

class MainComponent extends Component {
    render () {
        return (
          <div className={classes.mainComponent}>
              This is the main component.
          </div>
        );
    }
}

export default MainComponent;