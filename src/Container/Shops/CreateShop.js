import React, { Component } from 'react';
import ShopForm from "../../Components/Forms/ShopForm";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Spinner from "../../Components/Spinner/spinner";
import { Redirect } from 'react-router-dom';

class CreateShop extends Component {

    initialState = {
        initialValues: null,
        createdShopData: null,
        userData: null,
        shopData: null,
        updatedShopData: null
    };

    state = this.initialState;

    handleShop = (values) => {
        let valuesJson = JSON.parse(values);
        this.props.createShop(valuesJson);
    };

    updateUser = (values) => {
         let valuesJson = JSON.parse(values);
         this.props.updateShop(valuesJson, this.props.match.params.id);
    };

    componentDidMount () {
        this.props.getUsers();
    }

    componentWillUnmount() {
        this.setState({
                initialValues: null,
                createdShopData: null,
                userData: null,
                shopData: null,
                updatedShopData: null
            }
        );
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.userData !== this.state.userData) {
            this.setState({
                ...this.state,
                userData: nextProps.userData
            });
        }

        if (nextProps.shopsData && this.props.match.params.id > 0) {
            let shopData;
            for (let i=0; i< nextProps.shopsData.length; i++) {
                if (nextProps.shopsData[i].id === this.props.match.params.id) {
                    shopData = nextProps.shopsData[i];
                }
            }

            let initialValues = {
                shopName: shopData.shop_name,
                address: shopData.address,
                user: shopData.user_id
            };

            this.setState(
                {
                    ...this.state,
                    initialValues: initialValues,
                    shopData: shopData,
                    userData: nextProps.userData
                }
            );
        }

        if (nextProps.createdShopData !== this.state.createdShopData) {
                this.setState({
                    ...this.state,
                    createdShopData: nextProps.createdShopData
                });
        }

        if (nextProps.updatedShopData !== this.state.updatedShopData) {
            this.setState({
                ...this.state,
                updatedShopData: nextProps.updatedShopData
            });
        }
    }

    render () {

        if (this.state.createdShopData || this.state.updatedShopData) {

            return (
                <Redirect to="/shops"/>
            );
        }

        if (this.state.initialValues && this.state.userData) {
            return (
                <ShopForm onFormSubmit={(values) => this.updateUser(values)} initialValues={this.state.initialValues}
                          dropDownData={this.state.userData}/>
            );
        }

        if (!this.props.match.params.id && this.state.userData) {
            return (
                <ShopForm onFormSubmit={(values) => this.handleShop(values)} dropDownData={this.state.userData}/>
            );
        }

        return (
            <Spinner />
        );

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createShop: (values) => dispatch(actions.createShop(values)),
        getUsers: () => dispatch(actions.getUsers()),
        updateShop: (values, shopId) => dispatch(actions.updateShop(values, shopId)),
    }
};

const mapStateToProps = (state) => {
    return {
        userData: state.users.userData,
        shopsData: state.shops.shopsData,
        createdShopData: state.shops.createdShopData,
        updatedShopData: state.shops.updatedShopData
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( CreateShop );