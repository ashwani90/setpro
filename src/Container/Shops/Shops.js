import React, {Component} from 'react';
import CreateButton from '../../Components/Forms/Buttons/CreateButton';
import DataTable from '../../Components/Tables/DataTable';
import * as actions from '../../store/actions/index';
import Spinner from '../../Components/Spinner/spinner';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

const createShop = "Create Shop";

class Shops extends Component {

    state = {
        shopsData: null
    };

    componentDidMount() {
        (this.props.getShops());
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.shopsData !== this.state.shopsData) {
            this.setState(
                {
                    ...this.state,
                    shopsData: nextProps.shopsData
                }
            );
        }
    }

    updateActionCalled = (shopId) => {

        this.setState(
            {
                ...this.state,
                updateActionId: shopId
            }
        );
    };

    createButtonClicked = () => {
        this.setState(
            {
                ...this.state,
                createAction: true
            }
        );
    };

    componentWillUnmount() {
        this.setState({
            shopsData: null
            }
        );
    }

    render () {

        if (this.state.createAction) {
            return (
                <Redirect to={this.props.match.url + "/create"} />
            );
        }

        if (this.state.updateActionId) {
            return (
                <Redirect to={this.props.match.url + "/create/" + this.state.updateActionId} />
            );
        }

        if (this.state.shopsData !== null) {
            let allColumns = Object.keys(this.state.shopsData[0]);

            return (
                <div>
                    <CreateButton text={createShop} handleClick={this.createButtonClicked}/>
                    <DataTable allColumns={allColumns} data={this.state.shopsData}
                               updateAction={(shopId) => this.updateActionCalled(shopId)} />
                </div>
            );
        }

        return (
            <Spinner />
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getShops: () => dispatch(actions.getShops())
    }
};

const mapStateToProps = (state) => {
    return {
        shopsData: state.shops.shopsData,
        error: state.shops.error,
    };
};


export default connect ( mapStateToProps, mapDispatchToProps )(Shops);