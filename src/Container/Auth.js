import React, { Component } from 'react';
import { Button } from 'reactstrap';
import { LoginForm } from "../Components/Forms/loginForm";
import { RegisterForm } from "../Components/Forms/registerForm";
import * as actions from '../store/actions/index';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

class Auth extends Component {
    state = {
        isSignup: false,
        userId: null
    };

    handleSubmit = (values, isLogin) => {

        let valuesJson = JSON.parse(values);

        if (isLogin) {
            this.props.onAuth( valuesJson, isLogin );
        } else {
            this.props.onAuth( valuesJson, isLogin );
        }
    };

    switchAuthModeHandler = () => {
        this.setState( prevState => {
            return { isSignup: !prevState.isSignup };
        } );
    };

    render () {

        let authRedirect = null;

        if (this.props.isLoggedIn) {
            authRedirect = <Redirect to={this.props.authRedirectPath} />
        }

        return (
            <div>
                {authRedirect}
            { this.state.isSignup ?
                <RegisterForm onFormSubmit={ (values) => this.handleSubmit(values, false) }/> :
                <LoginForm onFormSubmit={ (values) => this.handleSubmit(values, true) } />
            }

            <Button
                onClick={this.switchAuthModeHandler}
                color="danger" type="button">SWITCH TO {this.state.isSignup ? 'SIGNIN' : 'SIGNUP'}</Button>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onAuth: ( values, isLogin ) => dispatch( actions.authLogin( values, isLogin ) ),
    };
};

const mapStateToProps = (state) => {
    return {
        isLoggedIn: state.auth.isLoggedIn,
        userId: state.auth.userId,
        authRedirectPath: state.auth.authRedirectPath
    };
};

export default connect( mapStateToProps , mapDispatchToProps )( Auth );