import React, { Component } from 'react';
import UserForm from "../../Components/Forms/UserForm";
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import { Redirect } from 'react-router-dom';
import Spinner from "../../Components/Spinner/spinner";

class CreateUser extends Component {

    state = {
        initialValues: null,
        userCreation: null,
        userUpdation: null
    };

    handleUser = (values) => {
         let valuesJson = JSON.parse(values);
         this.props.createUser(valuesJson);
    };

    updateUser = (values) => {
        let valuesJson = JSON.parse(values);
        this.props.updateUser(valuesJson, this.props.match.params.id);
    };

    componentDidMount () {
        if (this.props.match.params.id) {
            this.props.updateUserId(this.props.match.params.id);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.match.params.id &&
                nextProps.updateUserData) {
            let initialVal = {
                email: nextProps.updateUserData.email,
                password: nextProps.updateUserData.password,
                firstName: nextProps.updateUserData.firstName,
                lastName: nextProps.updateUserData.lastName,
                username: nextProps.updateUserData.username,
                address: nextProps.updateUserData.address,
                status: nextProps.updateUserData.status
            };

            this.setState({
                ...this.state,
                initialValues: initialVal
            });
        }


         if (nextProps.userCreation !== this.state.userCreation) {
             this.setState({
                 ...this.state,
                 userCreation: nextProps.userCreation
             });
         }

        if (nextProps.userUpdation !== this.state.userUpdation) {
            this.setState({
                ...this.state,
                userUpdation: nextProps.userUpdation
            });
        }

    }

    render () {

        if (this.state.userCreation || this.state.userUpdation) {

            return (
                <Redirect to="/userManagement"/>
            );
        }

        if (this.state.initialValues) {
            return (
                <UserForm onFormSubmit={(values) => this.updateUser(values)} initialValues={this.state.initialValues}/>
            );
        }

        if (! this.props.match.params.id) {
            return (
                <UserForm onFormSubmit={(values) => this.handleUser(values)}/>
            );
        }

        return (
            <Spinner />
        );

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createUser: (values) => dispatch(actions.createUser(values)),
        updateUser: (values, userId) => dispatch(actions.updateUser(values, userId)),
        updateUserId: (userId) => dispatch(actions.updateUserId(userId))
    }
};

const mapStateToProps = (state) => {
    return {
        userCreation: state.users.createdUserData,
        userCreationError:state.users.error,
        updateUserData: state.users.updateUserData,
        userUpdation: state.users.userUpdatedData
    };
};

export default connect( mapStateToProps, mapDispatchToProps )( CreateUser );