import React, { Component } from 'react';
import Sidebar from "../../Components/Navigation/Sidebar";
import Aux from './../Aux/Aux';
import { connect } from 'react-redux';
import  classes  from './Layout.css';

class Layout extends Component {
    render() {
        return(
            <Aux>
                <Sidebar />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    };
};

export default connect( mapStateToProps )( Layout );