import { Route } from 'react-router-dom';
import React from 'react';


export const CrudRoutes = ({ match } , firstComp, secondComp) => (
    <>
        <Route
            path={ match.url } exact
            component={ firstComp }
        />
        <Route
            path={ match.url + '/create/:id?' }
            component={ secondComp }
        />
    </>
);
