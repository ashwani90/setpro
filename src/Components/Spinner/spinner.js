import React, { Component } from 'react';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';

class Spinner extends Component {
    render () {
        return (
            <div className= {cx(globalStyles['spinner-border'], globalStyles['text-primary'])} role="status">
                <span className={cx(globalStyles['sr-only'])}>Loading...</span>
            </div>
        );
    }
}

export default Spinner;