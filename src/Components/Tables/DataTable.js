import React, { Component } from 'react';
import { Table } from 'reactstrap';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';

class DataTable extends Component {

    handleUpdateAction = (id) => {
        this.props.updateAction(id);
    };

    render () {
        return (
          <div>
              <Table className={globalStyles.table}>
                  <thead>
                  <tr>
                      <th>#</th>
                      {
                          this.props.allColumns.map( (step, index) => {
                              return (
                                    <th key={index}>{step.replace('_', " ").toLowerCase().replace(/\b[a-z]/g, function(letter) {
                                        return letter.toUpperCase();
                                    })}</th>
                              )
                              }
                          )
                      }
                  </tr>
                  </thead>
                  <tbody>
                      {

                          this.props.data.map( (stepParent, indexParent) => {
                              return (
                                  <tr key={indexParent}>
                                      <th scope="row">{indexParent+1}</th>
                                      {
                                          this.props.allColumns.map((step, index) => {
                                                  return (
                                                      <td key={index}>{stepParent[step]}</td>
                                                  );
                                              }
                                          )
                                      }
                                      <td> <button className={cx(globalStyles.btn, globalStyles['btn-warning'])}
                                      onClick={() => this.handleUpdateAction(stepParent['id'])}>
                                          Update
                                      </button> </td>

                                  </tr>

                              );

                              }

                          )

                      }


                  </tbody>
              </Table>
          </div>
        );
    }
}

export default DataTable;