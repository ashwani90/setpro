import React, { Component } from 'react';
import {
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import classes from './Sidebar.css';

class Sidebar extends Component {

    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle () {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render () {
        return (
            <div className={classes.sidenav}>
                <Navbar color="light" light expand="md" >
                    <NavbarBrand href="/">Dashboard</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Nav>
                        <NavItem>
                            <Link to="/userManagement">
                                User Management
                            </Link>
                        </NavItem>
                    </Nav>
                    <Nav>
                        <NavItem>
                            <Link to="/shops">
                                Shops
                            </Link>
                        </NavItem>
                    </Nav>
                    <Nav>
                        <NavItem>
                            <Link to="/products">
                                Products
                            </Link>
                        </NavItem>
                    </Nav>
                    <Nav>
                        <NavItem>
                            <Link to="/settings">
                                Settings
                            </Link>
                        </NavItem>
                    </Nav>
                </Navbar>
            </div>
        );
    }
}

export default Sidebar;