import React, { Component } from 'react';
import { Formik } from 'formik';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';
import {DropDownInput, TextInput} from "./FormComponents/FormComponents";

class ShopForm extends Component {


    render () {

        let  formValues;
        if (this.props.initialValues) {
            formValues = this.props.initialValues;
        } else {
            formValues = { shopName: '', address: '', user: 1 };
        }

        return (
            <div>
                <h1>Shop Form</h1>
                <Formik
                    initialValues={ formValues }
                    validate={values => {
                        let errors = {};
                        if (!values.shopName) {
                            errors.email = 'Required';
                        } else if (!values.address) {
                            errors.address = 'Required';
                        }

                        return errors;
                    }}

                    onSubmit={(values, {setSubmitting}) => {
                        values = JSON.stringify(values, null, 2);
                        this.props.onFormSubmit(values);
                        setSubmitting(false);
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                          /* and other goodies */
                      }) => (
                        <form onSubmit={handleSubmit}>

                            <TextInput
                                name="shopName"
                                id="shopName"
                                textValue={values.shopName}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="text"
                                placeholderValue="Enter shop Name"
                                labelValue="Shop Name"
                            />

                            {errors.shopName && touched.shopName && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}>  {errors.shopName} </div>}

                            <TextInput
                                name="address"
                                id="address"
                                textValue={values.address}
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="text"
                                placeholderValue="Enter Address"
                                labelValue="Address"
                            />

                            {errors.address && touched.address && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}>  {errors.address} </div>}

                            <DropDownInput
                                id = "user"
                                name = "user"
                                defaultValue = {formValues.user}
                                dropDownData = {this.props.dropDownData}
                                keyValue = "first_name"
                                handleChange = {handleChange}
                                handleBlur = {handleBlur}
                            />

                            <input type="submit" disabled={isSubmitting} className={cx(globalStyles.btn, globalStyles['btn-primary'])}
                                   value="Submit" />
                        </form>
                    )}
                </Formik>
            </div>
        );
    }

}

export default ShopForm;