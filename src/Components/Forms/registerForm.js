import React, {Component} from 'react';
import { Formik } from 'formik';
import { FormGroup, Label, Input, Button, Alert } from 'reactstrap';


export class RegisterForm extends Component {

    render () {
        return (
            <div>
                <h1>Register</h1>
                <Formik
                    initialValues={{email: '', password: '', first_name: '', last_name: '', username: '', address: ''}}
                    validate={values => {
                        let errors = {};
                        if (!values.email) {
                            errors.email = 'Required';
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                            errors.email = 'Invalid email address';
                        } else if (!values.password) {
                            errors.password = 'Required';
                        } else if (!values.first_name) {
                            errors.first_name = 'Required';
                        } else if (!values.last_name) {
                            errors.last_name = 'Required';
                        } else if (!values.username) {
                            errors.username = 'Required';
                        } else if (!values.address) {
                            errors.address = 'Required';
                        }
                        return errors;
                    }}

                    onSubmit={(values, {setSubmitting}) => {
                        values = JSON.stringify(values, null, 2);
                        this.props.onFormSubmit(values);
                        setSubmitting(false);
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                          /* and other goodies */
                      }) => (
                        <form onSubmit={handleSubmit}>

                            <FormGroup>
                                <Label for="first_name">First Name</Label>
                                <Input
                                    type="text"
                                    name="first_name"
                                    id="first_name"
                                    placeholder="Enter First Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.first_name}
                                />
                            </FormGroup>

                            {errors.first_name && touched.first_name && <Alert color="danger">  {errors.first_name} </Alert>}

                            <FormGroup>
                                <Label for="last_name">Last Name</Label>
                                <Input
                                    type="text"
                                    name="last_name"
                                    id="last_name"
                                    placeholder="Enter Last Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.last_name}
                                />
                            </FormGroup>

                            {errors.last_name && touched.last_name && <Alert color="danger">  {errors.last_name} </Alert>}

                            <FormGroup>
                                <Label for="username">Username</Label>
                                <Input
                                    type="text"
                                    name="username"
                                    id="username"
                                    placeholder="Enter User Name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.username}
                                />
                            </FormGroup>

                            {errors.username && touched.username && <Alert color="danger">  {errors.username} </Alert>}

                            <FormGroup>
                                <Label for="email">Email</Label>
                                <Input
                                    type="email"
                                    name="email"
                                    id="email"
                                    placeholder="Enter Email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                />
                            </FormGroup>

                            {errors.email && touched.email && <Alert color="danger">  {errors.email} </Alert>}

                            <FormGroup>
                                <Label for="password">Password</Label>
                                <Input
                                    type="password"
                                    name="password"
                                    id="password"
                                    placeholder="Enter Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                />
                            </FormGroup>

                            {errors.password && touched.password && <Alert color="danger"> {errors.password} </Alert>}

                            <FormGroup>
                                <Label for="address">Address</Label>
                                <Input
                                    type="text"
                                    name="address"
                                    id="address"
                                    placeholder="Enter Address"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.address}
                                />
                            </FormGroup>

                            {errors.address && touched.address && <Alert color="danger">  {errors.address} </Alert>}

                            <Button type="submit" disabled={isSubmitting}>
                                Submit
                            </Button>

                        </form>
                    )}
                </Formik>
            </div>
        );
    };
}


