import React, { Component } from 'react';
import { Formik } from 'formik';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';
import { TextInput, RadioInput } from "./FormComponents/FormComponents";

class UserForm extends Component {


    render () {

        let  formValues;
        if (this.props.initialValues) {

            formValues = this.props.initialValues;
        } else {
            formValues = { email: '', password: '', firstName: '', lastName: '', username: '', address: '', status: 'active' };
        }

        let radioObject = {
            radioData : [
                {
                    value: "active",
                    label: "Active",

                },
                {
                    value: "inactive",
                    label: "Inactive",

                }
            ]
        };

        return (
            <div>
                <h1>User Form</h1>
                <Formik
                    initialValues={ formValues }
                    validate={values => {
                        let errors = {};
                        if (!values.email) {
                            errors.email = 'Required';
                        } else if (
                            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                        ) {
                            errors.email = 'Invalid email address';
                        } else if (!values.password) {
                            errors.password = 'Required';
                        } else if (!values.firstName) {
                            errors.firstName = 'Required';
                        } else if (!values.lastName) {
                            errors.lastName = 'Required';
                        } else if (!values.username) {
                            errors.username = 'Required';
                        } else if (!values.address) {
                            errors.address = 'Required';
                        }
                        return errors;
                    }}

                    onSubmit={(values, {setSubmitting}) => {
                        values = JSON.stringify(values, null, 2);
                        this.props.onFormSubmit(values);
                        setSubmitting(false);
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                          /* and other goodies */
                      }) => (
                        <form onSubmit={handleSubmit}>
                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="text"
                                name="firstName"
                                id="firstName"
                                textValue={values.firstName}
                                placeholderValue="Enter first Name"
                                labelValue="First Name"
                            />

                            {errors.firstName && touched.firstName && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}>  {errors.firstName} </div>}

                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="text"
                                name="lastName"
                                id="lastName"
                                textValue={values.lastName}
                                placeholderValue="Enter last Name"
                                labelValue="Last Name"
                            />

                            {errors.lastName && touched.lastName && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}>  {errors.lastName} </div>}

                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="text"
                                name="username"
                                id="username"
                                textValue={values.username}
                                placeholderValue="Enter username"
                                labelValue="Username"
                            />

                            {errors.username && touched.username && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}> {errors.username} </div>}

                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="email"
                                name="email"
                                id="email"
                                textValue={values.email}
                                placeholderValue="Enter email address"
                                labelValue="Email"
                            />

                            {errors.email && touched.email && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}> {errors.email} </div>}

                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="password"
                                name="password"
                                id="password"
                                textValue={values.password}
                                placeholderValue="Enter Password"
                                labelValue="Password"
                            />

                            {errors.password && touched.password && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}> {errors.password} </div>}

                            <TextInput
                                handleChange={handleChange}
                                handleBlur={handleBlur}
                                inputType="address"
                                name="address"
                                id="address"
                                textValue={values.address}
                                placeholderValue="Enter address"
                                labelValue="Address"
                            />

                            {errors.address && touched.address && <div className={cx(globalStyles.alert, globalStyles['alert-danger'])}> {errors.address} </div>}

                            <RadioInput
                                dataObject = { radioObject }
                                inputType = "radio"
                                name = "status"
                                handleChange = { handleChange }
                                handleBlur = { handleBlur }
                                defaultValue = { formValues.status }
                            />

                            <input type="submit" disabled={isSubmitting} className={cx(globalStyles.btn, globalStyles['btn-primary'])}
                                   value="Submit" />
                        </form>
                    )}
                </Formik>
            </div>
        );
    }

}

export default UserForm;