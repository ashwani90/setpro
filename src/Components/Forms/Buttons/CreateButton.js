import React, { Component } from 'react';
import { Button } from 'reactstrap';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';

class CreateButton extends Component {

    render () {
        return(
            <Button className={cx(globalStyles.btn, globalStyles['btn-primary'])} onClick={() => this.props.handleClick()}>
                {this.props.text}
            </Button>
        );
    }
}

export default CreateButton;
