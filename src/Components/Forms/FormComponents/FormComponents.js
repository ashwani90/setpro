import React from 'react';
import PropTypes from 'prop-types';
import globalStyles from 'bootstrap/dist/css/bootstrap.min.css';
import cx from 'classnames';

export const TextInput = ({ name, id, textValue, handleChange, handleBlur, inputType, placeholderValue, labelValue }) => {
  return (
      <div className={cx(globalStyles['form-group'])}>
          <label htmlFor={id}>{ labelValue }</label>
          <input type={ inputType } className={ cx(globalStyles['form-control']) } id={ id }
                 name={ name } placeholder={ placeholderValue } onChange={ handleChange }
                 onBlur={ handleBlur } value={ textValue }/>
      </div>
  );
};

TextInput.propTypes = {
    name: PropTypes.string,
    labelValue: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.object
    ]),
    handleChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    inputType: PropTypes.string,
    textValue: PropTypes.string,
};

export const RadioInput = ( { dataObject, inputType, name, handleChange, handleBlur, defaultValue } ) => {

    return (
        <div>
            {
                dataObject.radioData.map((step, index) => {
                    return (
                        <label className={cx(globalStyles['radio-inline'])} key={index}>
                            <input type={inputType} name={name}
                                   onChange={handleChange} value={step.value}
                                   onBlur={handleBlur} defaultChecked={ step.value === defaultValue }/>
                            {step.label}
                        </label>
                    );
                }
            )
            }
        </div>
    );
};

RadioInput.propTypes = {
    name: PropTypes.string,
    handleChange: PropTypes.func.isRequired,
    dataObject: PropTypes.object,
    inputType: PropTypes.string,
};

export const DropDownInput = ( { id, name, defaultValue, dropDownData, keyValue, handleChange, handleBlur } ) => {
    return (
        <div className={cx(globalStyles['form-group'])}>
            <label htmlFor={id}>Select User:</label>
            <select name={name} id={id} className={cx(globalStyles['form-control'])}
                    defaultValue={defaultValue} onChange={handleChange} onBlur={handleBlur}
            >
                {
                    dropDownData.map((step, index) => {
                        return (
                            <option key={index} value={step.id}>
                                {step[keyValue]}
                            </option>
                        );
                    })
                }
            </select>
        </div>
    );
};

DropDownInput.propTypes = {
    name: PropTypes.string,
    handleChange: PropTypes.func.isRequired,
    dataObject: PropTypes.object,
    inputType: PropTypes.string,
};
