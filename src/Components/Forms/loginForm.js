import React, {Component} from 'react';
import { Formik } from 'formik';
import { FormGroup, Label, Input, Button, Alert } from 'reactstrap';


export class LoginForm extends Component {

    render () {
      return (
          <div>
              <h1>Sign In</h1>
              <Formik
                  initialValues={{email: '', password: ''}}
                  validate={values => {
                      let errors = {};
                      if (!values.email) {
                          errors.email = 'Required';
                      } else if (
                          !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
                      ) {
                          errors.email = 'Invalid email address';
                      } else if (!values.password) {
                          errors.password = 'Required';
                      }
                      return errors;
                  }}
                  onSubmit={(values, {setSubmitting}) => {
                      values = JSON.stringify(values, null, 2);
                      this.props.onFormSubmit(values);
                      setSubmitting(false);
                  }}
              >
                  {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,

                    }) => (
                      <form onSubmit={handleSubmit}>
                          <FormGroup>
                              <Label for="email">Email</Label>
                              <Input
                                  type="email"
                                  name="email"
                                  id="email"
                                  placeholder="Enter Email"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.email}
                              />
                          </FormGroup>

                          {errors.email && touched.email && <Alert color="danger">  {errors.email} </Alert>}

                          <FormGroup>
                              <Label for="password">Password</Label>
                              <Input
                                  type="password"
                                  name="password"
                                  id="password"
                                  placeholder="Enter Password"
                                  onChange={handleChange}
                                  onBlur={handleBlur}
                                  value={values.password}
                              />
                          </FormGroup>

                          {errors.password && touched.password && <Alert color="danger"> {errors.password} </Alert>}

                          <Button type="submit" disabled={isSubmitting}>
                              Submit
                          </Button>

                      </form>
                  )}
              </Formik>
          </div>
      );
    };
}


