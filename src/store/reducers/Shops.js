import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    shopsData: null,
    error: null,
    createdShopData: null,
    updatedShopData: null
};

const gotShops = (state, action) => {
    return updateObject( state, { shopsData: action.data, createdShopData: null, updatedShopData: null } );
};

const gotShopsError = (state, action) => {
    return updateObject( state, { error: action.error } );
};

const shopCreated = (state, action) => {
    return updateObject( state, { createdShopData: action.data, updatedShopData: null });
};

const shopUpdated = (state, action) => {
    return updateObject( state, { updatedShopData: action.data, createdShopData: null });
};

const reducer = (state = initialState, action ) => {
    switch (action.type) {
        case actionTypes.GOT_SHOPS: return gotShops(state, action);
        case actionTypes.GOT_SHOPS_ERROR: return gotShopsError(state, action);
        case actionTypes.SHOP_CREATED: return shopCreated(state, action);
        case actionTypes.SHOP_CREATED_ERROR: return gotShopsError(state, action);
        case actionTypes.SHOP_UPDATED: return shopUpdated(state, action);
        case actionTypes.SHOP_UPDATED_ERROR: return gotShopsError(state, action);
        default: return state;
    }
};

export default reducer;