import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    'isLoggedIn': false,
    'userId': null,
    'error': null,
    'authRedirectPath': null
};

const authFail = (state, action) => {
    return updateObject( state, { error: action.error, isLoggedIn: false } );
};

const authSuccess = (state, action) => {

    return updateObject( state, { userId: action.userId, isLoggedIn: true, authRedirectPath: '/' } );
};

const reducer = (state = initialState, action ) => {
    switch (action.type) {
        case actionTypes.AUTH_FAIL: return authFail(state, action);
        case actionTypes.AUTH_SUCCESS: return authSuccess(state, action);
        default: return state;
    }
};

export default reducer;