import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

const initialState = {
    userData: null,
    error: null,
    createdUserData: null,
    updateUserData: null,
    userUpdatedData: null
};

const gotUsers = (state, action) => {

    return updateObject( state, { userData: action.usersData, createdUserData: null,
        updateUserData: null, userUpdatedData: null } );
};

const gotUsersError = (state, action) => {
    return updateObject( state, { error: action.error } );
};

const userCreated = (state, action) => {

  return updateObject( state, { createdUserData: action.data } );
};

const userCreatedError = (state, action) => {
    return updateObject( state, { error: action.error } );
};

const gotUser = (state, action) => {
    return updateObject( state, { updateUserData: action.userData } );
};

const userUpdated = (state, action) => {
    return updateObject( state, { userUpdatedData: action.userUpdatedData } );
};

const reducer = (state = initialState, action ) => {
    switch (action.type) {
        case actionTypes.GOT_USERS: return gotUsers(state, action);
        case actionTypes.GOT_USERS_ERROR: return gotUsersError(state, action);
        case actionTypes.USER_CREATED: return userCreated(state, action);
        case actionTypes.USER_CREATED_ERROR: return userCreatedError(state, action);
        case actionTypes.GOT_USER: return gotUser(state, action);
        case actionTypes.GOT_USER_ERROR: return gotUsersError(state, action);
        case actionTypes.USER_UPDATED: return userUpdated(state, action);
        case actionTypes.USER_UPDATED_ERROR: return gotUsersError(state, action);
        default: return state;
    }
};

export default reducer;
