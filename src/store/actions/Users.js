import axios from 'axios';
import * as actionTypes from "./actionTypes";

export const getUsers = () => {

    return (dispatch) => {
        let url = "http://localhost/simple_react_demo/getUsers.php";
        axios.get(url)
            .then(
                response =>  {

                    dispatch(gotUsers(response.data.data));

                }
            ).catch(
                err => {
                    dispatch(gotUsersError(err.data));
                }
        )
    }
};

export const gotUsers = (usersData) => {
    return {
        type: actionTypes.GOT_USERS,
        usersData: usersData
    };
};

export const gotUsersError = (error) => {
    return {
        type: actionTypes.GOT_USERS_ERROR,
        error: error
    };
};

export const createUser = (values) => {
    return (dispatch) => {

        let url = "http://localhost/simple_react_demo/createUser.php";
        let userData = {
          'firstName': values.firstName,
          'lastName' : values.lastName,
          'username': values.username,
          'email': values.email,
          'address': values.address,
          'password': values.password,
          'status': values.status,
          'api_name': 'create_user_api'
        };
        axios.post(url, {userData})
            .then(
                response => {

                    dispatch(userCreated(response.data.data));
                }
            ).catch(
                err => {
                    dispatch(userCreatedError(err.data));
                }
        )
    }
};


export const userCreated = (data) => {
    return {
        type: actionTypes.USER_CREATED,
        data: data
    }
};

export const userCreatedError = (error) => {
  return {
      type: actionTypes.USER_CREATED_ERROR,
      error: error
  }
};

export const updateUserId = (userId) => {

    return (dispatch) => {
        let url = "http://localhost/simple_react_demo/getUser.php?id=" + userId;
        axios.get(url)
            .then(
                response =>  {
                    dispatch(gotUser(response.data));
                }
            ).catch(
            err => {
                dispatch(gotUserError(err.data));
            }
        )
    }
};

export const gotUser = (userData) => {
    return {
        type: actionTypes.GOT_USER,
        userData: userData
    }
};

export const gotUserError = (error) => {
    return {
        type: actionTypes.GOT_USER_ERROR,
        error: error
    }
};

export const updateUser = (values, userId) => {
    return (dispatch) => {

        let url = "http://localhost/simple_react_demo/updateUser.php";
        let userData = {
            'userId': userId,
            'firstName': values.firstName,
            'lastName' : values.lastName,
            'username': values.username,
            'email': values.email,
            'address': values.address,
            'password': values.password,
            'status': values.status,
            'api_name': 'update_user_api'
        };
        axios.put(url, {userData})
            .then(
                response => {
                    console.log(response.data.data);
                    dispatch(userUpdated(response.data.data));
                }
            ).catch(
            err => {
                dispatch(userUpdatedError(err.data));
            }
        )
    }
};

export const userUpdated = (data) => {
    return {
      type: actionTypes.USER_UPDATED,
      userUpdatedData: data
    };
};

export const userUpdatedError = (error) => {
    return {
        type: actionTypes.USER_UPDATED_ERROR,
        error: error
    };
};

