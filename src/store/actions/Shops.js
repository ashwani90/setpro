import axios from "axios/index";
import * as actionTypes from "./actionTypes";

export const getShops = () => {
    return (dispatch) => {
        let url = "http://localhost/simple_react_demo/getShops.php";
        axios.get(url)
            .then(
                response =>  {

                    dispatch(gotShops(response.data.data));

                }
            ).catch(
            err => {
                dispatch(gotShopsError(err.data));
            }
        )
    }
};

export const gotShops = (shopsData) => {
    return {
        type: actionTypes.GOT_SHOPS,
        data: shopsData
    };
};

export const gotShopsError = (error) => {
    return {
        type: actionTypes.GOT_SHOPS_ERROR,
        error: error
    };
};

export const createShop = (values) => {
    return (dispatch) => {

        let url = "http://localhost/simple_react_demo/createShop.php";
        let shopData = {
            'shopName': values.shopName,
            'address': values.address,
            'user': values.user,
            'api_name': 'create_shop_api'
        };

        axios.post(url, { shopData })
            .then(
                response => {
                    dispatch(shopCreated(response.data.data));
                }
            ).catch(
            err => {
                dispatch(shopCreatedError(err.data));
            }
        )
    }
};

export const shopCreated = (shopData) => {
    return {
        type: actionTypes.SHOP_CREATED,
        data: shopData
    }
};

export const shopCreatedError = (error) => {
    return {
        type: actionTypes.SHOP_CREATED_ERROR,
        data: error
    }
};

export const updateShop = (values, shopId) => {
    return (dispatch) => {

        let url = "http://localhost/simple_react_demo/updateShop.php";
        let shopData = {
            'shopId': shopId,
            'shopName': values.shopName,
            'address': values.address,
            'user_id': values.user,
            'api_name': 'update_shop_api'
        };
        axios.put(url, {shopData})
            .then(
                response => {
                    dispatch(shopUpdated(response.data.data));
                }
            ).catch(
            err => {
                dispatch(shopUpdatedError(err.data));
            }
        )
    }
};

export const shopUpdated = (data) => {
    return {
        type: actionTypes.SHOP_UPDATED,
        data: data
    }
};


export const shopUpdatedError = (error) => {
    return {
        type: actionTypes.SHOP_UPDATED_ERROR,
        data: error
    }
};