import axios from 'axios';

import * as actionTypes from './actionTypes';

export const authLogin = (values, isLogin) => {
    return dispatch => {

        let authData, url;
            if (isLogin) {
                authData = {
                    'email': values.email,
                    'password': values.password,
                    'api_name': 'login_api'
                };
                url = "http://localhost/simple_react_demo/login.php";
            } else {
                authData = {
                    'first_name': values.first_name,
                    'last_name': values.last_name,
                    'username': values.username,
                    'address': values.address,
                    'email': values.email,
                    'password': values.password,
                    'api_name': 'register_api'
                };
                url = "http://localhost/simple_react_demo/register.php";
            }

            axios.post(url, {authData})
                .then(response => {
                    localStorage.setItem('userId', response.data.data.id);
                    localStorage.setItem('first_name', response.data.data.first_name);
                    localStorage.setItem('last_name', response.data.data.last_name);
                    localStorage.setItem('username', response.data.data.username);
                    localStorage.setItem('email', response.data.data.email);
                    localStorage.setItem('address', response.data.data.address);

                    dispatch(authSuccess(response.data.data.id));
                }).catch(err => {
                    dispatch(authFail(err.response.data.error));
                    console.log(err);
            });
        }
};

export const authSuccess = (userId) => {

    return {
       type: actionTypes.AUTH_SUCCESS,
       userId: userId
    };
};

export const authFail = (error) => {
    return {
        type: actionTypes.AUTH_FAIL,
        error: error
    };
};