import React, { Component } from 'react';
import './App.css';
import Layout from './hoc/Layout/Layout';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import asyncComponent from './hoc/asyncComponent';
import { CrudRoutes } from './hoc/Route/CrudRoutes';

const asyncAuth = asyncComponent(() => {
    return import('./Container/Auth');
});

const asyncCreateUser = asyncComponent(() => {
    return import('./Container/User/CreateUser');
});

const asyncUserManagement = asyncComponent(() => {
    return import('./Container/UserManager');
});

const asyncShops = asyncComponent(() => {
    return import('./Container/Shops/Shops');
});

const asyncProducts = asyncComponent(() => {
    return import('./Container/Products');
});

const asyncSettings = asyncComponent(() => {
    return import('./Container/Settings');
});

const asyncDashboard = asyncComponent(() => {
    return import('./Container/Dashboard');
});

const asyncLifeCycleTest = asyncComponent(() => {
    return import('./Container/ComponentLifeCycle');
});

const asyncCreateShop = asyncComponent(() => {
    return import('./Container/Shops/CreateShop');
});

class App extends Component {

    render() {
        let routes = (
            <Switch>
                <Route path="/auth" component={asyncAuth}/>
                <Route path="/" exact component={asyncDashboard}/>
                
                <Route path="/userManagement"
                       component={({ match }) => CrudRoutes( { match }, asyncUserManagement, asyncCreateUser )}
                />
                <Route path="/shops"
                       component={({ match }) => CrudRoutes( { match }, asyncShops, asyncCreateShop )}
                />

                <Route path="/products" exact component={asyncProducts}/>
                <Route path="/settings" exact component={asyncSettings}/>
                <Route path="/life" exact component={asyncLifeCycleTest}/>
                <Redirect to="/"/>
            </Switch>
        );

        if (this.props.isAuthenticated) {
            routes = (
                <Switch>
                    <Route path="/auth" component={asyncAuth}/>
                    <Route path="/" exact component={asyncDashboard}/>
                    <Redirect to="/"/>
                </Switch>
            );
        }

            return (
                <div className="App">
                    <Layout>
                        {routes}
                    </Layout>
                </div>
            );
    }
}
export default withRouter(App);
